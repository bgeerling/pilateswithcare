var gulp = require('gulp');
var gulpSass = require('gulp-sass');
var dartSass = require('sass')
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var browserSync = require('browser-sync').create();

// Copy third party libraries from /node_modules into /vendor
gulp.task('vendor', function (done) {

  // Bootstrap
  gulp.src([
      './node_modules/bootstrap/dist/**/*',
      '!./node_modules/bootstrap/dist/css/bootstrap-grid*',
      '!./node_modules/bootstrap/dist/css/bootstrap-reboot*'
    ])
    .pipe(gulp.dest('./vendor/bootstrap'))

  // Font Awesome
  gulp.src([
      './node_modules/font-awesome/**/*',
      '!./node_modules/font-awesome/{less,less/*}',
      '!./node_modules/font-awesome/{scss,scss/*}',
      '!./node_modules/font-awesome/.*',
      '!./node_modules/font-awesome/*.{txt,json,md}'
    ])
    .pipe(gulp.dest('./vendor/font-awesome'))

  // jQuery
  gulp.src([
      './node_modules/jquery/dist/*',
      '!./node_modules/jquery/dist/core.js'
    ])
    .pipe(gulp.dest('./vendor/jquery'))

  // jQuery Easing
  gulp.src([
      './node_modules/jquery.easing/*.js'
    ])
    .pipe(gulp.dest('./vendor/jquery-easing'))

  // Magnific Popup
  gulp.src([
      './node_modules/magnific-popup/dist/*'
    ])
    .pipe(gulp.dest('./vendor/magnific-popup'))

  // Scrollreveal
  gulp.src([
      './node_modules/scrollreveal/dist/*.js'
    ])
    .pipe(gulp.dest('./vendor/scrollreveal'))
    
  done();
});

// Compile SCSS
var sass = gulpSass(dartSass)
gulp.task('css:compile', gulp.series(function() {
  return gulp.src('./scss/**/*.scss')
    .pipe(sass.sync({
      outputStyle: 'expanded'
    }).on('error', sass.logError))
    .pipe(gulp.dest('./css'))
}));

// Minify CSS
gulp.task('css:minify', gulp.series(['css:compile'], function() {
  return gulp.src([
      './css/*.css',
      '!./css/*.min.css'
    ])
    .pipe(cleanCSS())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('./css'))
    .pipe(browserSync.stream());
}));

// CSS
gulp.task('css', gulp.series(['css:compile', 'css:minify']));

// Minify JavaScript
gulp.task('js:minify', gulp.series(function() {
  return gulp.src([
      './js/*.js',
      '!./js/*.min.js'
    ])
    .pipe(uglify())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('./js'))
    .pipe(browserSync.stream());
}));

// JS
gulp.task('js', gulp.series(['js:minify']));

// Default task
gulp.task('default', gulp.series(['css', 'js', 'vendor']));

// Default task
gulp.task('build', gulp.series(['css', 'js', 'vendor'], function(done) {
  gulp.src(['./css/**.min.css']).pipe(gulp.dest('./dist/css'));
  gulp.src(['./js/**.min.js']).pipe(gulp.dest('./dist/js'));
  gulp.src(['./img/**/*']).pipe(gulp.dest('./dist/img'));
  gulp.src(['./vendor/**/**.min.css']).pipe(gulp.dest('./dist/vendor'));
  gulp.src(['./vendor/**/**.min.js']).pipe(gulp.dest('./dist/vendor'));
  gulp.src(['./vendor/font-awesome/fonts/*']).pipe(gulp.dest('./dist/vendor/font-awesome/fonts'));
  gulp.src(['./**.html']).pipe(gulp.dest('./dist'));
  gulp.src(['sitemap.xml']).pipe(gulp.dest('./dist'));
  gulp.src(['robots.txt']).pipe(gulp.dest('./dist'));
  done();
}));

// Configure the browserSync task
gulp.task('browserSync', gulp.series(function() {
  browserSync.init({
    server: {
      baseDir: "./"
    }
  });
}));

// Dev task
gulp.task('dev', gulp.series(['css', 'js', 'browserSync'], function() {
  gulp.watch('./scss/*.scss', ['css']);
  gulp.watch('./js/*.js', ['js']);
  gulp.watch('./*.html', browserSync.reload);
}));
